/* See LICENSE file for copyright and license details. */

/* {} will be substituted with the actual argument when the rule is executed */
struct rule rules[] = {
	{ .regex = "\\.(avi|mp4|mkv|mp3|ogg|flac|mov)$", .file = "mpv", .argv = { "mpv", "{}", NULL } },
	{ .regex = "\\.(png|jpg|gif)$", .file = "nsxiv", .argv = { "nsxiv", "{}", NULL} },
	{ .regex = "\\.(html|svg)$", .file = "surf", .argv = { "surf", "{}", NULL } },
	{ .regex = "\\.pdf$", .file = "zathura", .argv = { "zathura", "{}", NULL} },
	{ .regex = "\\.sh$", .file = "sh", .argv = { "sh", "{}", NULL} },
	{ .regex = ".", .file = "less", .argv = { "less", "{}", NULL } },
};

